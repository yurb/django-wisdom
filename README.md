A minimal django app for managing units of (text or other) content. The app is designed to play nice with rich tect editors in admin (via the [`WISDOM_ADMIN_TEXT_WIDGET` setting](#setting)) as well as with [`django-modeltranslation`][modeltranslation], although neither is required to use it.

# Installation

Install the package, then add `wisdom` to your `INSTALLED_APPS` and run migrations.

# Usage

## Models

Theis app provides one abstract base model and one concrete model based on it:
- `BaseContent` - the abstract model contains common fields for all potential content item models:
  - `identifier` - a unique (human-readable) identifier for the content item;
  - `title` - a title for the item, to be displayed on frontend if desired;
  - `created` and `updated` - timestamps set and updated automatically when the item is created and updated respectively.

- `TextContent` - a concrete model, based on `BaseContent`, adding a single `body` field of type text.

One can potentially define their own content types based on `BaseContent` if need arises.

## Displaying content

You can render one or more content items on a page using the `wisdom`
template tag.

To render the value of `body` field of a content item whose `identifier` is `my_identifier`, do:

```html
{% load wisdom %}
{% wisdom "my_identifier" %}
```

You may also access any field (as well as potentially transform it
using filters) via the following syntax:

```html
{% load wisdom %}
{% wisdom "my_identifier" as content %}
{{ content.title }}: {{ content.body|truncatechars:300 %}
```

Alternatively, if you need to display a specific single content item
on a page as its primary content, you can use the `ContentView` as a
convenience.

Add the desired page to your urlconf:

```python
from django.urls import path
from wisdom.views import ContentView

urlpatterns = (
    # ...,
    path(
        "about/team",
        ContentView.as_view(identifier="about-team", template_name="myproject/about.html"),
        name="about-team"
    )
    # ...
)
```

This will render `myproject/about.html` with the `content` context variable set to the insteance of `TextContent` having the `about-team` identifier.

To instead load a dynamic content item based on url args, you can omit `identifier`. In this case, the view will behave identically to `DetailView` with `slug_field` set to `identifier` and `slug_url_kwarg` left with its default value of `slug`.

## Using rich text widgets in admin

<a id="setting" />

In order to use a custom widget for the `body` field in admin, for example `TinyMCE` from [django-tinymce](https://github.com/jazzband/django-tinymce), set the `WISDOM_ADMIN_TEXT_WIDGET` setting to string path of the desired widget class:

```python
WISDOM_ADMIN_TEXT_WIDGET = "tinymce.widgets.TinyMCE"
```



[modeltranslation]: https://pypi.org/project/django-modeltranslation/
