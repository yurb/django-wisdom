import pytest
from django.http import Http404
from django.template import Context, Template
from wisdom.models import TextContent
from wisdom.views import ContentView


@pytest.fixture
def content_item(db):
    content = TextContent.objects.create(
        identifier="my_content",
        title="A title",
        body="Lorem ipsum, as they say.",
    )
    return content


def test_contentview(content_item, rf):
    request = rf.get("/")
    view = ContentView.as_view(template_name="none", identifier="my_content")
    response = view(request)

    assert (
        "content" in response.context_data
        and response.context_data["content"] == content_item
    )


@pytest.mark.django_db
def test_contentview_404(rf):
    request = rf.get("/")
    view = ContentView.as_view(template_name="none", identifier="does-not-exist")
    with pytest.raises(Http404):
        view(request)


def test_contentview_dynamic_url(content_item, rf):
    request = rf.get("/")
    view = ContentView.as_view(template_name="none")
    response = view(request, slug="my_content")

    assert response.context_data["content"] == content_item


def test_template_tag(content_item):
    out = Template(
        # fmt: off
        "{% load wisdom %}"
        "{% wisdom 'my_content' %}"
        # fmt: on
    ).render(Context())

    assert out == "Lorem ipsum, as they say."


def test_template_tag_var(content_item):
    out = Template(
        # fmt: off
        "{% load wisdom %}"
        "{% wisdom 'my_content' as content %}"
        "{{ content.title }}: {{ content.body }}"
        # fmt: on
    ).render(Context())

    assert out == "A title: Lorem ipsum, as they say."


def test_template_tag_notfound_debug_true(content_item, settings):
    settings.DEBUG = True
    out = Template(
        # fmt: off
        "{% load wisdom %}"
        "{% wisdom 'missing' %}"
        # fmt: on
    ).render(Context())

    assert "`missing` does not exist." in out


def test_template_tag_notfound_debug_false(content_item, settings):
    settings.DEBUG = False
    out = Template(
        # fmt: off
        "{% load wisdom %}"
        "{% wisdom 'missing' %}"
        # fmt: on
    ).render(Context())

    assert out == ""


def test_custom_editing_widget(content_item, settings):
    settings.WISDOM_ADMIN_TEXT_WIDGET = "test.widget.CustomWidget"
    from wisdom.forms import TextContentForm
    from .widget import CustomWidget
    form = TextContentForm()
    assert form.base_fields["body"].widget.__class__ is CustomWidget
