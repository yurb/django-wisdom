DEBUG = True
SECRET_KEY = "testing"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
    }
}

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "APP_DIRS": True,
    }
]

INSTALLED_APPS = ["wisdom"]
