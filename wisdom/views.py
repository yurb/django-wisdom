from django.core.exceptions import ImproperlyConfigured
from django.shortcuts import get_object_or_404
from django.views.generic.detail import DetailView

from .models import TextContent


class ContentView(DetailView):
    """Load a specific predefined content item if `identifier` class
    attribute is set, otherwise behave like DetailView.

    The content item is provided as `content` context object in template."""

    model = TextContent
    identifier = None
    context_object_name = "content"
    slug_field = "identifier"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.template_name:
            raise ImproperlyConfigured(
                f"Specifying template_name is required in {self.__class__}"
            )

    def get_object(self, *args, **kwargs):
        if self.identifier:
            return get_object_or_404(self.model, identifier=self.identifier)
        else:
            return super().get_object(*args, **kwargs)
