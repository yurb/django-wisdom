from django import forms
from django.conf import settings
from django.utils.module_loading import import_string

from .models import TextContent

_DEFAULT_TEXT_WIDGET = forms.Textarea


def get_text_widget():
    """Load and return a user-specified widget class or the default widget otherwise."""
    widget_setting = getattr(settings, "WISDOM_ADMIN_TEXT_WIDGET", None)
    if widget_setting:
        if type(widget_setting) is str:
            return import_string(widget_setting)
        else:
            return widget_setting
    else:
        return _DEFAULT_TEXT_WIDGET


class TextContentForm(forms.ModelForm):
    """Allows overriding the widget used for the body field
    using the `WISDOM_BODY_WIDGET` setting."""

    class Meta:
        model = TextContent
        fields = (
            "identifier",
            "title",
            "body",
        )
        widgets = {"body": get_text_widget()()}
