from django.contrib import admin

from .models import TextContent
from .forms import TextContentForm


@admin.register(TextContent)
class TextContentAdmin(admin.ModelAdmin):
    form = TextContentForm
    list_display = ("identifier", "title", "created", "updated")
