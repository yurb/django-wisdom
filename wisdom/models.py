from django.db import models
from django.utils.translation import gettext_lazy as _


class BaseContent(models.Model):
    class Meta:
        abstract = True
        ordering = ('identifier',)

    identifier = models.SlugField(
        verbose_name=_("Content identifier"),
        help_text=_("Internal identifier, not presented to the viewers."),
        unique=True,
    )

    title = models.CharField(
        blank=True,
        max_length=1000,
        verbose_name=_("Content title"),
        help_text=_("Title of the content, possibly presented to the viewer."),
    )

    created = models.DateTimeField(
        auto_now_add=True,
        verbose_name=_("Created on"),
    )
    updated = models.DateTimeField(
        auto_now=True,
        verbose_name=_("Updated on"),
    )

    def __str__(self):
        if self.title:
            return f"{self.title} ({self.identifier})"
        else:
            return self.identifier


class TextContent(BaseContent):
    class Meta:
        verbose_name_plural = _("Text content items")

    body = models.TextField(verbose_name=_("Content body"))
