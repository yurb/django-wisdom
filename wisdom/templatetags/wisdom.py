from classytags.arguments import Argument
from classytags.core import Options, Tag
from django import template
from django.conf import settings
from django.utils.translation import gettext as _

from ..models import TextContent

register = template.Library()

#
# TODO: Think about ways of allowing the user to specify desired model
# on case-by-case basis. Perhaps via an alias defined in settings?
#
_MODEL = TextContent


class Wisdom(Tag):
    name = "wisdom"

    options = Options(
        Argument("identifier"),
        "as",
        Argument("varname", required=False, resolve=False),
    )

    def render_tag(self, context, identifier, varname):
        try:
            obj = _MODEL.objects.get(identifier=identifier)
            if varname:
                context[varname] = obj
                return ""
            else:
                return obj.body
        except _MODEL.DoesNotExist:
            if settings.DEBUG and not varname:
                return _(
                    f"Warning: Content with identifier `{identifier}` does not exist."
                )
            elif varname:
                return None
            else:
                return ""


register.tag(Wisdom)
