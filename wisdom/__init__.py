"""A minimal django app for managing units of (text or other) content,
designed to play nice with modeltranslation and rich text widgets in
django admin.
"""

__version__ = "0.1.2"
